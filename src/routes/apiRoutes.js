const router = require('express').Router()
const teacherController = require('../controller/teacherController')
const alunoController = require('../controller/alunoController')
const JSONPlaceholderController = require('../controller/JSONPlaceholderController')

router.post('/alunoCadastrato/', alunoController.postAluno)
router.put('/alunoCadastrato/', alunoController.updateAluno)
router.delete('/deleteAluno/:id', alunoController.deleteAlunos)
router.get("/external/",JSONPlaceholderController.getUsers)
router.get("/external/io",JSONPlaceholderController.getUsersWebSiteIO)
router.get("/external/com",JSONPlaceholderController.getUsersWebSiteCOM)
router.get("/external/net",JSONPlaceholderController.getUsersWebSiteNET)
router.get('/external/filter',JSONPlaceholderController.getCountDomain)


module.exports = router